package org.catness.prophecy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.TextView
import android.view.inputmethod.InputMethodManager
import android.content.Context        
import android.widget.GridLayout
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import org.catness.prophecy.databinding.ActivityMainBinding

/*  NOTE!!!
    Each layout file then has its own generated data binding class. 
    The Jetpack library does the job for you. By default, the class name 
    is the name of the layout file in Pascal case and with the word binding at the end. 
*/
/*
 So, the underscores in the names are important - they are perceived as separators for capitalizing the words inside whole name.
*/

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val prophecy: Prophecy = Prophecy("Something will happen...")
    lateinit var wordFields : Array<EditText>
    var curProphecy : Int = 0
    var numProphecies : Int = prophecies.size

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
/* NOTE!!!
    When the binding object is created, the compiler generates the names of the views in the 
    binding object from the IDs of the views in the layout, converting them to camel case. 
    So, for example, done_button is doneButton in the binding object, 
    nickname_edit becomes becomes nicknameEdit, and nickname_text becomes nicknameText.
*/
        // we can omit the binding. prefix if we enclose all the following in binding.apply {}
        binding.apply {
            wordFields = arrayOf(noun1,noun2,noun3,noun4,verb1,adjective1,adjective2)

            revealButton.setOnClickListener {
                revealProphecy(it)
            }
            resetButton.setOnClickListener {
                resetProphecy(it)
            }
        }   
        binding.prophecy = prophecy
    }

    private fun revealProphecy(view: View) {
        // Read the text from all the text fields and convert it to a list of strings.
        // If the text is empty, replace it with a default string, which depends on the position in the array.
        val lst = wordFields.mapIndexed { ind, it -> it.text.toString().ifEmpty { defaults[ind] } }
        
        // we are not updating the text view in the layout!
        // here  we're setting the prophecy data class variable to the new value
        // and the layout will be updated automatically because of data binding
        // just don't forget to call binding.invalidateAll()
        prophecy?.text = generateProphecy(lst,curProphecy) 
        if (++curProphecy >= numProphecies) curProphecy = 0
        binding.apply {
            formLayout.visibility = View.GONE
            resultLayout.visibility = View.VISIBLE
            invalidateAll()
        }
    }

    private fun resetProphecy(view: View) {
        binding.apply {
            formLayout.visibility = View.VISIBLE
            resultLayout.visibility = View.GONE
        }
    }  



}