package org.catness.prophecy

/*
Prophecy templates must be written with spaces after } and punctuation, otherwise they are not split correctly into words.
To fix the punctuation in the end, use replace with Regex, which deletes extra spaces.
*/
data class Prophecy(var text: String = "") // for the data binding exercise

val prophecies: List<String> = listOf(
    "When {adjective1} {noun1} will {verb1} , a {adjective2} {noun2} shall cause an eternal {noun3} and an end to the {noun4} .",
    "It shall be then, when the {noun1} turns {adjective1} , the {noun2} shall {verb1} the age of {noun3} and the rise of a {adjective2} {noun4} .",
    "There comes a day when the {noun1} will {verb1} the {adjective1} {noun2} , the {adjective2} one shall bring the return of the {noun3} and an age of the {noun4} .",
    "Upon the day the {noun1} becomes shrouded in shadows, the {adjective1} {noun2} shall {verb1} and bring forth the fall of a {adjective2} {noun3} and an age of the {noun4} ."
    )

val defaults: List<String> = listOf("moon","battle","night","kingdom","appear","red","furious")

fun generateProphecy(params : List<String>, num : Int) : String {
/*
    I couldn't find how to do named parameter substitution in string formatting.
    So I adapted this method from:
    https://stackoverflow.com/questions/59775037/kotlin-is-there-a-base-function-to-replace-multiple-strings-with-multiple-stri
 */
   
    val keywords = listOf("{noun1}","{noun2}","{noun3}","{noun4}","{verb1}","{adjective1}","{adjective2}")
    val transform = keywords.zip(params).toMap()
    val words = prophecies[num].split(' ')
    val result = words.map { transform[it] ?: it}.joinToString(separator = " ")
    return result.replace(Regex(" +(\\,|\\.)"),"$1")
}
